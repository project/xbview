*****************************************************************************
                                X B   V I E W
*****************************************************************************
Name: xbview module
Author: Thilo Wawrzik <drupal at profix898 dot de>
Drupal: 6.x
*****************************************************************************
DESCRIPTION:

The xbview module is a simple module to display bookmarks through a dhtml
(html/css + javascript) frontend. It is designed to read bookmarks from
a XML Bookmark Exchange Language (XBEL) file.

XBEL [1] is a xml-based format to store and exchange bookmarks. Various
applications can handle XBEL, esp. open-source software like Konqueror
and Galeon browsers. For Firefox users there is an addon available [2,3]
to export bookmarks directly to webspace (e.g. ftp). You can then use
XBView to view these bookmarks from within Drupal.

XBView uses a simple css stylesheet to format the output, what makes it
easy to customize the layout.

You can view an online demo (german only) of this module at
http://www.profix898.de/en/bookmarks

*****************************************************************************
INSTALLATION:

1. Place whole xbview folder into your Drupal modules/ directory.

2. Enable the xbview module by navigating to
     administer > modules
     
3. Bring up xbview configuration screen by navigating to
     administer > settings > xbview
   Enter (at least) the path to your bookmark xml file and optionally
   configure all other options as well.

*****************************************************************************
[1] XBEL InfoPage: http://pyxml.sourceforge.net/topics/xbel/
[2] Bookmarks Synchronizer 3: https://addons.mozilla.org/firefox/1989/
[3] Bookmark Sync and Sort: https://addons.mozilla.org/de/firefox/addon/2367
Sample XBEL file: http://www.profix898.de/files/profix898/bookmarks.xml
