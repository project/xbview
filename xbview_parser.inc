<?php

function _xbview_parse() {
  if (!($bookmark_path = _xbview_xmlpath())) {
    return FALSE;
  }
  $xbview_parser = new xbview_parser();
  
  return $xbview_parser->parse($bookmark_path);
}

/**
 * CLASS: xbview_parser
 */
class xbview_parser {

  var $parser = NULL;
  var $title = FALSE;
  var $element = array();
  var $stack = array();
  var $count = 1;
  
  function parse($xmlfile) {
    $xmldata = file_get_contents($xmlfile);
    if (!$xmldata) {
      drupal_set_message(t('Unable to open bookmark file!'), 'error');
      return FALSE;
    }
    
    $xmldata = preg_replace('@<title[^>]*>(.*?)</title>@i', '<title><![CDATA[\1]]></title>', $xmldata);
    
    $this->parser = xml_parser_create('UTF-8');
    xml_set_object($this->parser, $this);
    xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 1);
    xml_set_element_handler($this->parser, 'xbel_tag_open', 'xbel_tag_close');
    xml_set_character_data_handler($this->parser, 'xbel_tag_data');
 
    if (!xml_parse($this->parser, $xmldata)) {
      drupal_set_message('XML error: '.
        xml_error_string(xml_get_error_code($this->parser)) .' at line '.
        xml_get_current_line_number($this->parser)
      );
      return FALSE;
    }
    
    xml_parser_free($this->parser);
    
    drupal_set_message(t('XBEL file successfully parsed.'));
    return TRUE;
  }
  
  function xbel_tag_open($parser, $name, $attrs) {
    switch ($name) {
      case 'XBEL':
        db_lock_table('xbookmark');
        db_query("DELETE FROM {xbookmark} ");
        db_unlock_tables();
        $this->element = array('type' => XBVIEW_NONE, 'id' => 0);
        break;
      case 'TITLE':
        $this->title = TRUE;
        break;
      case 'METADATA':
        $this->element['icon'] = isset($attrs['ICON']) ? $attrs['ICON'] : '';
        break;
      case 'FOLDER':
        array_push($this->stack, $this->element);
        $this->element['type'] = XBVIEW_FOLDER;
        $this->element['href'] = '';
        $this->element['icon'] = '';
        $this->element['parent'] = $this->element['id'];
        //$this->element['id'] = $attrs['ID'] ? crc32($attrs['ID']) : $this->count++;
        $this->element['id'] = $this->count++;
        break;
      case 'BOOKMARK':
        array_push($this->stack, $this->element);
        $this->element['type'] = XBVIEW_BOOKMARK;
        $this->element['href'] = $attrs['HREF'];
        $this->element['icon'] = '';
        $this->element['parent'] = $this->element['id'];
        //$this->element['id'] = $attrs['ID'] ? crc32($attrs['ID']) : $this->count++;
        $this->element['id'] = $this->count++;
        break;
    }
  }
  
  function xbel_tag_data($parser, $data) {
    if (trim($data) && $this->title) {
      $this->element['title'] = htmlspecialchars($data);
    }
  }
  
  function xbel_tag_close($parser, $name) {
    switch ($name) {
      case 'XBEL':
        variable_set('xbview_title', $this->element['title']);
        $this->element = array('type' => XBVIEW_NONE);
        break;
      case 'TITLE':
        $this->title = FALSE;
        break;
      case 'FOLDER':
      case 'BOOKMARK':
        db_lock_table('xbookmark');
        db_query("INSERT INTO {xbookmark} (id, title, href, icon, type, parent) VALUES ('%d', '%s', '%s', '%s', '%s', '%d')",
          $this->element['id'], $this->element['title'], $this->element['href'], $this->element['icon'], $this->element['type'], $this->element['parent']);
        db_unlock_tables();
        $this->element = array_pop($this->stack);
        break;
    }
  }
}
