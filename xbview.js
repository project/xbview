
Drupal.behaviors.xbview = function (context) {
  $('a.xb-folder', context).each(function() {
    $(this).bind('click', function() {
      Drupal.xbviewFolder($(this).attr('id').substr(3));
      return false;
    });
  });
};

Drupal.xbviewFolder = function(id) {
  folder = $('#folder-'+id);
  if (folder.html() == '') {
    $('body').css('cursor', 'wait');
    $.ajax({
      url: Drupal.settings.xbview.callback+id,
      error: function(http){
        alert(Drupal.t('HTTP Request Failure (@status)', { '@status': http.status }));
      },
      success: function(data){
        folder.hide().html(data).slideDown('normal');
        Drupal.attachBehaviors('#folder-'+id);
      },
      complete: function(data){
         $('body').css('cursor', 'default');
      }
    });
  } else {
    folder.slideUp('normal', function(){ $(this).empty(); });
  }
};
