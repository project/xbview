<?php

function _xbview_admin_form() {
  $form['xbview'] = array(
    '#type' => 'fieldset',
    '#title' => t('XBView Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );  
  $form['xbview']['xbview_disclaimer'] = array(
    '#type' => 'textarea',
    '#title' => t('Disclaimer'),
    '#rows' => 3,
    '#default_value' => variable_get('xbview_disclaimer', ''),
    '#description' => t('Leave empty to disable/hide disclaimer.'),
  );
  
  $form['xbview']['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $sort_options = array(
    XBVIEW_SORT_ABC => t('Alphabetically'),
    XBVIEW_SORT_ABC_FOLDER => t('Alphabetically (folders first)'),
    XBVIEW_SORT_XBEL => t('Original XBEL order'),
    XBVIEW_SORT_XBEL_FOLDER => t('Original XBEL order (folders first)'),
  );
  $form['xbview']['display']['xbview_sort'] = array(
    '#type' => 'select',
    '#title' => t('Sort order'),
    '#default_value' => variable_get('xbview_sort', XBVIEW_SORT_ABC_FOLDER),
    '#options' => $sort_options,
    '#description' => t('Select how the bookmarks/folders should be sorted.'),
  );
  $form['xbview']['display']['xbview_icons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display bookmark icons'),
    '#default_value' => variable_get('xbview_icons', 1),
    '#description' => t('Read icon urls from file and display icons for bookmarks.'),
  );
  $form['xbview']['display']['xbview_hide_toplevel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide toplevel bookmarks'),
    '#default_value' => variable_get('xbview_hide_toplevel', 0),
    '#description' => t('Show bookmarks in (sub)folders only.'),
  );
  $form['xbview']['display']['xbview_javascript'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Javascript navigation'),
    '#default_value' => variable_get('xbview_javascript', 1),
  );
  $form['xbview']['display']['xbview_link_target'] = array(
    '#type' => 'textfield',
    '#title' => t('Link target'),
    '#default_value' => variable_get('xbview_link_target', '_blank'),
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t('Enter a link target (e.g. "_blank", "_new").'),
  );
  
  $form['xbview']['parse'] = array(
    '#type' => 'fieldset',
    '#title' => t('XBEL Parse Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Click on the following link to parse the XBEL file: <a href="@xbel-parse">Update now</a>', array('@xbel-parse' => url('admin/settings/xbview/parse'))),
  );
  $form['xbview']['parse']['xbview_bookmark_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to XBEL bookmark file'),
    '#default_value' => variable_get('xbview_bookmark_path', 'files/bookmarks.xml'),
    '#size' => 70,
    '#maxlength' => 255,
    '#description' => t('Relative path from document root, e.g. \'files/bookmarks.xml\', or absolute URL starting with \'http://...\'')
  );
  $period = drupal_map_assoc(array(0, 300, 600, 900, 1800, 2700, 3600, 10800, 21600, 32400, 43200), 'format_interval');
  $period[0] = t('Manually');
  $form['xbview']['parse']['xbview_cron_interval'] = array(
    '#type' => 'select',
    '#title' => t('Update interval'),
    '#default_value' => variable_get('xbview_cron_interval', 0),
    '#options' => $period,
    '#description' => t('Select interval bookmarks are updated from the XBEL file. Choose \'Manually\' to disable cron updates.'),
  );
  $lastrun = format_date(variable_get('xbview_cron_lastrun', 0));
  $form['xbview']['parse']['lastrun'] = array('#value' => t('Last update: @lastrun', array('@lastrun' => $lastrun)));
  
  $form['#validate'][] = '_xbview_admin_validate';
  
  return system_settings_form($form);
}

function _xbview_admin_validate($form, &$form_state) {
  if (!empty($form_state['values']['xbview_bookmark_path'])) {
    if (!_xbview_xmlpath($form_state['values']['xbview_bookmark_path'])) {
      form_set_error('xbview_bookmark_path');
    }
  }
}
